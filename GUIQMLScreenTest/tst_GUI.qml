import QtQuick 2.0
import QtTest 1.0
import "../FibonacciPgm"

TestCase {

    id: testCase
    when: windowShown
    width: 480
    height: 800

    property FibonacciScreen aFibscreen  :null
    property MockBusinessLogic business : MockBusinessLogic{}

    Component {
        id:factory
        FibonacciScreen{

        }
    }

    function init()
    {
        aFibscreen = factory.createObject(parent)

    }

    function cleanup()
    {
        aFibscreen.destroy()
    }


    function test_testmousePress()
    {
        //Prepare   
        aFibscreen.currentInputIndex = 10
        aFibscreen.width= 100
        aFibscreen.height = 100

        //Simulate
        mousePress( aFibscreen )

        //Compare
        compare ( aFibscreen.fibRectangle.x, 0 )
    }


}

