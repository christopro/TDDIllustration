import QtQuick 2.0
import QtTest 1.0
import "../FibonacciPgm"

TestCase {
    property Fibrect fibrect  :null

    Component {
        id:factory

        Fibrect{

        }

    }

    function init()
    {

        fibrect = factory.createObject(parent)


    }

    function test_smalfont()
    {
        //Prepare
        fibrect.width = 20

        compare( 10 , fibrect.textLabel.font.pixelSize)
    }


    function test_bigfont()
    {
        //Prepare
        fibrect.width = 120

        compare( 32 , fibrect.textLabel.font.pixelSize)
    }


    function cleanup()
    {
        fibrect.destroy()
        fibrect = null
    }

}
