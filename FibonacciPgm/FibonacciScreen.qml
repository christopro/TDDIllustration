import QtQuick 2.0
import QtQuick.Window 2.2

Item {
    id: fibScreen

    property var currentInputIndex: 0

    property Component fibComponent: null
    property Fibrect fibRectangle: null

    property int scaleQuotient: 64


    function drawFibRec(input)
    {
        var theXCoordinate
        var theYCoordinate
        var theWidthofRectangle = Qt.binding( function() { return business.fib(input) * (fibScreen.width/scaleQuotient) } )

        if( input % 2 != 0)
        {

            theXCoordinate=0
            theYCoordinate =  Qt.binding( function() {
                return fibScreen.height  - business.fib(input) * (fibScreen.width/scaleQuotient) - (business.fib(input-1)*(fibScreen.width/scaleQuotient)) } )

        }
        else
        {
            theXCoordinate = Qt.binding( function() { return business.fib(input-1) * (fibScreen.width/scaleQuotient) } )
            theYCoordinate = Qt.binding( function() { return fibScreen.height - business.fib(input) * (fibScreen.width/scaleQuotient) } )
        }


        fibComponent = Qt.createComponent("Fibrect.qml")
        fibRectangle = fibComponent.createObject(fibScreen, {x:theXCoordinate,
                                                             y:theYCoordinate,
                                                             width: theWidthofRectangle,
                                                             text: input
                                                             })


        currentInputIndex = currentInputIndex + 1

    }



    MouseArea{
        anchors.fill: parent

        onPressed:{

            drawFibRec(currentInputIndex+1)
        }
    }

}
