import QtQuick 2.0

Rectangle {

    id: fibRect
    property alias text: aText.text
    property alias textLabel: aText
    color: "lightblue"

    height: width
    border.color: "black"

    Text
    {   id: aText

        anchors.centerIn: parent
        font.pixelSize: fibRect.width > 32 ? 32 :  fibRect.width/2
    }
}
